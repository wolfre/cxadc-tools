#!/bin/bash

source source-common.sh


zip -r "${SLUG}-$SEMVER-$(uname -m).zip" "$DIR_OUTPUT" 
echo "Done packaging"
