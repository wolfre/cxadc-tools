#!/bin/bash

source source-common.sh

function build
{
	local src="$1"
	local out="$2"

	test -d "$src" || die "'$src' is not a dir"

	local files="$(find "$src" -iname "*.c" -type f)"
	gcc \
		-Wall -Werror=incompatible-pointer-types -Werror=int-conversion \
		-oS -g \
		-I "$src" -o "$out" $files || die "Compilation of '$src' -> '$out' failed"

	echo "Compile '$src' -> '$out' done"
}



mkdir -p "$DIR_OUTPUT"
build "$DIR_SRC_BASE/show-level" "$DIR_OUTPUT/cxadc-show-level"
echo "Done building"
