# cxadc tools

Some cli tools to work with [cxadc][cxadc] modified capture cards.

## cxadc-show-level

Starts a capture on a [cxadc][cxadc] device and displays the levels as ascii art.

```
./cxadc-show-level /dev/cxadc0 u16 14318000 1000

#################################++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#################################++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#################################++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
################################++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
###############################+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
###############################++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
################################+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
###############################++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
###############################+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
```

The above example will open `/dev/cxadc0` and expects the card to output unsigned 16 bit samples (you have to configure those via the sysfs parameters first).
For each measurement `14318000` samples will be taken (about 1 sec of data on a card with stock crystal) and one bar will be drawn.
The bars will have a maximum width of `1000` chars if the signal is covering the full ADC range.

The graph shows two levels:
- the thick one `###` is the average level of the signal
- the thin one '+++' is the peak level of the signal

## building it

Basically run `build.sh` no special requirements besides gcc.
Also have a look at `.gitlab-ci.yml`.

[cxadc]: https://github.com/happycube/cxadc-linux3/
