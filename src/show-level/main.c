#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>

typedef enum
{
	sample_format_u8,
	sample_format_u16,
}
sample_format;

typedef struct
{
	char* cxadc_dev;
	sample_format fmt;
	int report_interval;
	int max_width;
}
cli_arguments;


static void parse_args(cli_arguments* parsed, int argc, char **argv)
{
	if( argc < 4)
	{
		fputs("Need at least 3 arguments: <device> <u8|u16> <report_interval> [max width]\n", stderr);
		fputs("e.g.: /dev/cxadc0 u8  2863636\n", stderr);
		fputs("e.g.: /dev/cxadc0 u16 1431818 500\n", stderr);
		exit(-1);
	}
	
	parsed->cxadc_dev = argv[1]; // TODO copy to local buf
	
	if( strcmp("u8", argv[2]) == 0)
	{
		parsed->fmt = sample_format_u8;
	}
	else if( strcmp("u16", argv[2]) == 0)
	{
		parsed->fmt = sample_format_u16;
	}
	else
	{
		printf("Can't parse '%s' into a format\n", argv[2]);
		exit(-1);
	}
	
	parsed->report_interval =  atoi(argv[3]);
	
	parsed->max_width = 80;
	if( argc > 4 )
		parsed->max_width = atoi(argv[4]);
}

typedef struct
{
	float peak;
	float avg;
}
level_measurement;

#define max_s16 0x7fff
#define max_s8 0x7f


static int calc_dc_off_u16(int len, uint16_t* buffer)
{
	int64_t dc_off=0;
	for(int i=0; i<len; ++i)
		dc_off += buffer[i];
	dc_off /= len;
	return dc_off;
}

static int calc_dc_off_u8(int len, uint8_t* buffer)
{
	int64_t dc_off=0;
	for(int i=0; i<len; ++i)
		dc_off += buffer[i];
	dc_off /= len;
	return dc_off;
}

static void calc_level_u16(level_measurement* result, int len, uint16_t* buffer)
{
	int dc_off = calc_dc_off_u16(len, buffer);
	int peak=0;
	int64_t avg=0;
	
	for(int i=0; i<len; ++i)
	{
		int v = buffer[i];
		v -= dc_off;
		
		if( v < 0)
			v *= -1;
		
		if( peak<v )
			peak = v;
		
		avg += v;
	}
	
	avg /= len;
	
	result->peak = peak;
	result->peak /= max_s16;
	
	result->avg = avg;
	result->avg /= max_s16;
}

static void calc_level_u8(level_measurement* result, int len, uint8_t* buffer)
{
	int dc_off = calc_dc_off_u8(len, buffer);
	int peak=0;
	int64_t avg=0;
	
	for(int i=0; i<len; ++i)
	{
		int v = buffer[i];
		v -= dc_off;
		
		if( v < 0)
			v *= -1;
		
		if( peak<v )
			peak = v;
		
		avg += v;
	}
	
	avg /= len;
	
	result->peak = peak;
	result->peak /= max_s16;
	
	result->avg = avg;
	result->avg /= max_s16;
}

static int max_width;
static char* bar_fat;
static char* bar_skinny;

static void prep_level_bar(int width)
{
	max_width = width;
	bar_fat = malloc(max_width);
	bar_skinny = malloc(max_width);
	
	for(int i=0; i<max_width; ++i)
	{
		bar_fat[i] = '#';
		bar_skinny[i] = '+';
	}
}

static void paint_level_bar(level_measurement* result)
{
	int skinny = (int)(result->peak * (float)max_width);
	int fat = (int)(result->avg * (float)max_width);
	
	skinny -=fat;
	
	if( fat > 0)
		fwrite(bar_fat, 1, fat, stdout);
	
	if( skinny > 0)
		fwrite(bar_skinny, 1, skinny, stdout);
	
	putc('\n', stdout);
}

static void run(cli_arguments* parsed)
{
	int buffer_size = (parsed->fmt == sample_format_u8 ? sizeof(uint8_t) : sizeof(uint16_t)) * parsed->report_interval;
	void* buffer = malloc(buffer_size);
	
	errno = 0;
	FILE * fp = fopen (parsed->cxadc_dev, "rb");
	if( fp == NULL || errno != 0 )
	{
		printf("Unable to open '%s': %s\n", parsed->cxadc_dev, strerror(errno));
		exit(-1);
	}
	
	level_measurement result;
	
	while(1)
	{
		int read = fread(buffer, buffer_size, 1, fp);
		if( read < 1 )
		{
			printf("EOF '%s'\n", parsed->cxadc_dev);
			exit(-1);
		}
		
		if( parsed->fmt == sample_format_u8 )
		{
			calc_level_u8(&result, parsed->report_interval, (uint8_t*)buffer);
		}
		else
		{
			calc_level_u16(&result, parsed->report_interval, (uint16_t*)buffer);
		}
		
		paint_level_bar(&result);
	}
}


int main(int argc, char **argv)
{
	cli_arguments parsed;
	
	parse_args(&parsed, argc, argv);
	printf("Will use %s %s with %d reporting interval and max width %d\n", parsed.cxadc_dev, parsed.fmt == sample_format_u8 ? "u8" : "u16", parsed.report_interval, parsed.max_width);
	
	prep_level_bar(parsed.max_width);
	run(&parsed);
	
	return 0;
}
